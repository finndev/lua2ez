local game_metatable = {
   __index = function(table, key)
      if key == "CursorPos" then
         return Vector3(Game.CursorPosX(), Game.CursorPosY(), Game.CursorPosZ())
      end
   end
}
--setmetatable(Game, game_metatable)

class 'Vector3'
--[
   function Vector3:__init(x, y, z)
      self.X = x
      self.Y = y
      self.Z = z
   end

   function Vector3:__type()
      return "Vector3"
   end

   function Vector3:__add(v)
      return Vector3(self.X + v.X, (v.Y and self.Y) and self.Y + v.Y, (v.Z and self.Z) and self.Z + v.Z)
   end

   function Vector3:__sub(v)
      return Vector3(self.X - v.X, (v.Y and self.Y) and self.Y - v.Y, (v.Z and self.Z) and self.Z - v.Z)
   end

   function Vector3:__eq(v)
      return self.X == v.X and self.Y == v.Y and self.Z == v.Z
   end

   function Vector3:__tostring()
      return string.format("(X: %g Y: %g Z: %g)", self.X, self.Y, self.Z)
   end
--]
